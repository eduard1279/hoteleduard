'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Habitaciones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      precioPorNoche: {
        type: Sequelize.INTEGER
      },
      piso: {
        type: Sequelize.INTEGER
      },
      maxPersonas: {
        type: Sequelize.INTEGER
      },
      tieneCamaBebe: {
        type: Sequelize.STRING
      },
      tieneDucha: {
        type: Sequelize.STRING
      },
      tieneBano: {
        type: Sequelize.STRING
      },
      tieneBalcon: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Habitaciones');
  }
};