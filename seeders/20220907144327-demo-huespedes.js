'use strict';

    module.exports = {
      async up (queryInterface, Sequelize) {
        return queryInterface.bulkInsert('Huespedes', [{
          nombre: "Eduard", 
          apellido: "Pillimue Flor",
          telefono: "3128676381",
          correo: "eduard1279@gmail.com",
          direccion: "carrera 6 # 31N - 134",
          ciudad: "Popayán",
          Pais: "Colombia",
          createdAt: new Date(),
          updatedAt: new Date()
        }]);
      },
    
      async down (queryInterface, Sequelize) {
        return queryInterface.bulkDelete('Huespedes', null, {});
        /**
         * Add commands to revert seed here.
         *
         * Example:
         * await queryInterface.bulkDelete('People', null, {});
         */
      }
    };
