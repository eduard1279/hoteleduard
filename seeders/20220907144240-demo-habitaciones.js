'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Habitaciones', [{
      precioPorNoche: 50000, 
      piso: 3,
      maxPersonas: 3,
      tieneCamaBebe: "Si",
      tieneDucha: "Si",
      tieneBano: "Si",
      tieneBalcon: "Si",
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Habitaciones', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};