var express = require('express');
var router = express.Router();

const huespedControllers = require('../controllers/huespedControllers');
const habitacionesControllers = require('../controllers/habitacionesControllers');
const reservaControllers = require('../controllers/reservaControllers');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/huespedes/',huespedControllers.List);
router.get('/api/huespedes/nombre/:nombre',huespedControllers.ListAt);

router.get('/api/habitaciones/',habitacionesControllers.List);
router.get('/api/habitaciones/id/:id',habitacionesControllers.ListAt);

router.get('/api/reservas/',reservaControllers.List);
router.get('/api/reservas/id/:id',reservaControllers.ListAt);

module.exports = router;


